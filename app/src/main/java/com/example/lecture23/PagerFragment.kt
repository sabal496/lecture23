package com.example.lecture23

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_pager.view.*
import java.text.FieldPosition

class PagerFragment(var image: Int,var position:Int) : Fragment() {

       lateinit var itemview:View

 override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

          itemview=inflater.inflate(R.layout.fragment_pager, container, false)
            setimages()
          return itemview
    }

    private fun setimages(){
        itemview.imagerec.setImageResource(image)
        itemview.position.text="position : "+position.toString()

    }

}
